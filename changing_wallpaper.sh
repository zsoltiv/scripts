#!/bin/sh

time=30

while true; do
	for img in $@; do
		feh --bg-scale $img
		sleep $time
	done
done
