#!/bin/sh

# change this to whatever you want
RESOLUTION='1080'
USER_AGENT='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36'

if [ -n $1 ]; then
    echo "Usage: $0 [SEARCH]"
    exit 1
fi

RARBG_BASE_URL='https://rarbg.to'
RARBG_URL="$RARBG_BASE_URL/torrents.php"

response=`curl -s -X GET -G \
	       -A $USER_AGENT $RARBG_URL --data-urlencode $1 \
	       | xmllint --format --html - 2> /dev/null`

torrents=`grep "<a onmouseover=\".\+\".\+title=\".\+$RESOLUTIONp.\+\[rartv\]\">"`

OIFS=$IFS

IFS='
'
