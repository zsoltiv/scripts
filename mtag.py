#!/usr/bin/python

"""
this script is used to set the artist and title of a music file
"""

import sys

try:
    import mutagen
except ImportError as err:
    print("failed to import mutagen")
    sys.exit(1)


def main():
    if len(sys.argv) != 4:
        print("usage: mtag.py [FILE] [ARTIST] [TITLE]")
        sys.exit(1)

    song = mutagen.File(sys.argv[1], easy = True)
    if song == None:
        print("failed to load file")
        sys.exit(1)

    song["artist"] = sys.argv[2]
    song["title"] = sys.argv[3]

    song.save(sys.argv[1])


if __name__ == "__main__":
    main()
