#!/bin/sh

SCRIPTS_DIR="$HOME/projects/scripts"
BRANCH="master"

cd "$SCRIPTS_DIR"
# push executable files to git
find . -maxdepth 1 -executable -type f -exec git add {} \;
git commit
git push origin "$BRANCH"
