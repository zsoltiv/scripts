#!/bin/bash

# shell script to get the
# currently playing song
# in cmus

if cmus-remote -Q | grep -q 'tag';
then
	artist=$(cmus-remote -Q | grep 'tag artist' | cut -d ' ' -f 3-)
	title=$(cmus-remote -Q | grep 'tag title' | cut -d ' ' -f 3-)
	album=$(cmus-remote -Q | grep 'tag album ' | cut -d ' ' -f 3-)
	echo "唄: $artist - $title ($album)"
else
	echo ""
fi
exit 1
