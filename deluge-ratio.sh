#!/bin/sh

scale=2

deluge_config=~/.config/deluge

total_download=$(awk '/total_download/{print $2}' $deluge_config/stats.totals | cut -d ',' -f 1)
total_upload=$(awk '/total_upload/{print $2}' $deluge_config/stats.totals | cut -d ',' -f 1)

echo $(echo "scale=$scale; $total_upload / $total_download" | bc)
