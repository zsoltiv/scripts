#!/bin/sh

ffmpeg \
	-f x11grab \
	-s $(xdpyinfo | awk '/dimensions/{print $2}') \
	-i :0.0 \
	-f pulse \
	-i default \
	-vcodec libx264 \
	output.mkv
